package advert

type ListRequest struct {
}

type CreateRequest struct {
	Advert Ad
}

type UpdateRequest struct {
	ID     string
	Advert Ad
}

type DeleteRequest struct {
	ID string
}

type GetByIdRequest struct {
	ID string
}
