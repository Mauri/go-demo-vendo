package advert

type ListResponse struct {
	Ads map[string]Ad `json:"ads"`
	Err error         `json:"error,omitempty"`
}

func (r ListResponse) error() error { return r.Err }

type CreateResponse struct {
	Err error `json:"error,omitempty"`
}

func (r CreateResponse) error() error { return r.Err }

type UpdateResponse struct {
	Err error `json:"error,omitempty"`
}

func (r UpdateResponse) error() error { return r.Err }

type DeleteResponse struct {
	Err error `json:"error,omitempty"`
}

func (r DeleteResponse) error() error { return r.Err }

type GetByIdResponse struct {
	Advert Ad    `json:"advert"`
	Err    error `json:"error,omitempty"`
}

func (r GetByIdResponse) error() error { return r.Err }
