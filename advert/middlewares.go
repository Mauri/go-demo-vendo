package advert

import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
)

type Middleware func(AdvertService) AdvertService

func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next AdvertService) AdvertService {
		return &loggingMiddleware{
			next:   next,
			logger: logger,
		}
	}
}

type loggingMiddleware struct {
	next   AdvertService
	logger log.Logger
}

func (mw loggingMiddleware) List(ctx context.Context) (ads map[string]Ad, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "List", "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.List(ctx)
}

func (mw loggingMiddleware) Create(ctx context.Context, advert Ad) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "Create", "id", advert.ID, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.Create(ctx, advert)
}

func (mw loggingMiddleware) Update(ctx context.Context, id string, advert Ad) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "Update", "id", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.Update(ctx, id, advert)
}

func (mw loggingMiddleware) Delete(ctx context.Context, id string) (err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "Delete", "id", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.Delete(ctx, id)
}

func (mw loggingMiddleware) GetById(ctx context.Context, id string) (advert Ad, err error) {
	defer func(begin time.Time) {
		mw.logger.Log("method", "GetById", "id", id, "took", time.Since(begin), "err", err)
	}(time.Now())
	return mw.next.GetById(ctx, id)
}
