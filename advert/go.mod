module vendo/advert

go 1.15

replace vendo.com/advert => ../advert

require (
	github.com/go-kit/kit v0.10.0
	github.com/gorilla/mux v1.7.3
	github.com/satori/go.uuid v1.2.0
	vendo.com/advert v0.0.0-00010101000000-000000000000
)
