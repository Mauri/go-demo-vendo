package advert

import (
	"context"
	"errors"
	"sync"

	uuid "github.com/satori/go.uuid"
)

type AdvertService interface {
	List(ctx context.Context) (map[string]Ad, error)
	Create(ctx context.Context, advert Ad) error
	Update(ctx context.Context, id string, advert Ad) error
	Delete(ctx context.Context, id string) error
	GetById(ctx context.Context, id string) (Ad, error)
}

type AdvertRepository interface {
	Find(ctx context.Context) (map[string]Ad, error)
	Create(ctx context.Context, advert Ad) error
	Update(ctx context.Context, id string, advert Ad) error
	Delete(ctx context.Context, id string) error
	GetById(ctx context.Context, id string) (Ad, error)
}

type Ad struct {
	ID       string  `json:"id"`
	Headline string  `json:"headline,omitempty"`
	Price    float64 `json:"price,omitempty"`
	Body     string  `json:"body,omitempty"`
}

var (
	ErrInconsistentIDs = errors.New("inconsistent IDs")
	ErrAlreadyExists   = errors.New("already exists")
	ErrNotFound        = errors.New("not found")
)

type inMemoryRepository struct {
	mtx sync.RWMutex
	ads map[string]Ad
}

func NewInMemoryRepository() AdvertRepository {
	return &inMemoryRepository{
		ads: map[string]Ad{},
	}
}

func (r *inMemoryRepository) Find(ctx context.Context) (map[string]Ad, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	return r.ads, nil
}

func (r *inMemoryRepository) Create(ctx context.Context, advert Ad) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	if _, ok := r.ads[advert.ID]; ok {
		return ErrAlreadyExists
	}
	r.ads[advert.ID] = advert
	return nil
}

func (r *inMemoryRepository) Update(ctx context.Context, id string, advert Ad) error {
	if id != advert.ID {
		return ErrInconsistentIDs
	}
	r.mtx.Lock()
	defer r.mtx.Unlock()
	r.ads[id] = advert
	return nil
}

func (r *inMemoryRepository) Delete(ctx context.Context, id string) error {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	if _, ok := r.ads[id]; !ok {
		return ErrNotFound
	}
	delete(r.ads, id)
	return nil
}

func (r *inMemoryRepository) GetById(ctx context.Context, id string) (Ad, error) {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	ad, ok := r.ads[id]
	if !ok {
		return Ad{}, ErrNotFound
	}
	return ad, nil
}

type advertService struct {
	repository AdvertRepository
}

func NewService(repository AdvertRepository) AdvertService {
	return &advertService{
		repository: repository,
	}
}

func (s *advertService) List(ctx context.Context) (map[string]Ad, error) {
	ads, err := s.repository.Find(ctx)
	if err != nil {
		return nil, err
	}
	return ads, nil
}

func (s *advertService) Create(ctx context.Context, advert Ad) error {
	uuid := uuid.NewV4()
	id := uuid.String()
	advert.ID = id

	if err := s.repository.Create(ctx, advert); err != nil {
		return err
	}
	return nil
}

func (s *advertService) Update(ctx context.Context, id string, advert Ad) error {
	ad, err := s.repository.GetById(ctx, id)
	if err != nil {
		return err
	}

	ad.Headline = advert.Headline
	ad.Price = advert.Price
	ad.Body = advert.Body

	if err := s.repository.Update(ctx, id, ad); err != nil {
		return err
	}
	return nil
}

func (s *advertService) Delete(ctx context.Context, id string) error {
	if err := s.repository.Delete(ctx, id); err != nil {
		return err
	}
	return nil
}

func (s *advertService) GetById(ctx context.Context, id string) (Ad, error) {
	ad, err := s.repository.GetById(ctx, id)
	if err != nil {
		return ad, err
	}
	return ad, nil
}
